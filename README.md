# video-balancer #

* Simple video balancer based on aiohttp.

## Usage ##

```bash
DEBUG=1 python -m balancer
```

or with Docker:

```bash
docker-compose up
```
