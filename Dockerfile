FROM python:3.7
COPY . /balancer
WORKDIR ./balancer
RUN pip install -r requirements.txt
CMD ["python", "-m", "balancer"]
