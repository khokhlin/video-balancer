import os
from urllib.parse import urlparse
from urllib.parse import urljoin
from aiohttp import web


DEBUG = os.environ.get("DEBUG", False)
VIDEO_NOT_FOUND_MSG = "Video not found"
CDN_HOST = os.environ.get("CDN_HOST", "http://localhost:80")


def to_cdn_url(url_param):
    parse_result = urlparse(url_param)
    subdomain, _ = parse_result.netloc.split(".", 1)
    path = subdomain + parse_result.path
    video_url = urljoin(CDN_HOST, path)
    return video_url


def url_maker():
    CDN_REQUESTS = 10
    request_counter = 1
    def inner(url_param):
        nonlocal request_counter
        if request_counter >= CDN_REQUESTS:
            request_counter = 1
            video_url = to_cdn_url(url_param)
        else:
            request_counter += 1
            video_url = url_param
        return video_url
    return inner


make_url = url_maker()


async def handler(request):
    url_param = request.rel_url.query.get("video")
    if not url_param:
        return web.json_response({"error": VIDEO_NOT_FOUND_MSG})
    else:
        video_url = make_url(url_param)
        if DEBUG:
            print(video_url)
        raise web.HTTPFound(video_url)


app = web.Application()
app.add_routes([
    web.get('/', handler),
])
